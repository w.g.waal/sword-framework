## Semi-automated WORkaround Detection (SWORD) framework

## Description
This is the R code accompanying the paper "Putting the SWORD to the Test: Finding Workarounds with Process Mining". It shows the implementation of the SWORD framework which can be used to detect potential workarounds through deviations from normative processes using specific patterns. 

## Usage
The createFullReport function can be used to run all patterns and provide the user with an overview of all potential workarounds in their data. This function applies all patterns to the provided event log and outputs a data frame containing a ranking for all patterns (except for the "Occurrence of directly repeating activity" pattern. See the paper for details.) It requires an event log in the form of a data frame with:
- a case id titled "CaseID"-
- an activity label titled "Event"
- a timestamp titled "Timestamp"
- a time of logging titled "Logtime"
- a resource titled "Resource"
- (optionally) a resource type titled "Type".

Note that these names are cases sensitive.

There are three optional arguments to this function:
- delta_z - Default=3, this determines the cutoff rate for reporting deviations. Higher values only report deviations that are further away from the mean. 
- printDetails - Default=FALSE, Set this to true to report individual trace deviations
- printReport - Default=TRUE, Set this to false to stop reporting the number of detected deviations based on delta_z per pattern

All patterns can also be called individually, see the createFullReport function and commentary for details.

## Support
This code is provided as-is. Unless there are large issues with it, there are no updates planned. Nevertheless, please contact Wouter van der Waal (w.g.vanderwaal@uu.nl) for any support for the SWORD framework.
